extends Node

#var websocket_url = "ws://161.35.201.51:3000"
var websocket_url = "ws://localhost:3000"

var ws = null

var is_connected = false
var hooks = {}

func _ready():
	ws = WebSocketClient.new()
	ws.connect("connection_established", self, "_connection_established")
	ws.connect("connection_closed", self, "_connection_closed")
	ws.connect("connection_error", self, "_connection_error")
	ws.connect("data_received", self, "_on_data")
		
func _process(delta):
	ws.poll()
	
func _on_data():
	var message = ws.get_peer(1).get_packet().get_string_from_utf8()
	print(message)
	var m_list = JSON.parse(message).result
	var event_name = m_list.pop_front()
	if hooks.has(event_name):
		for f in hooks[event_name]:
			if f.is_valid():
				f.call_funcv(m_list)
	if hooks.has('any'):
		for f in hooks['any']:
			if f.is_valid():
				f.call_func(event_name)
	_hooks_cleanup()
		
func _connection_established(protocol):
	is_connected = true
	if hooks.has('connect'):
		for f in hooks['connect']:
			f.call_func(protocol)
	
func _connection_closed(clean):
	is_connected = false
	if hooks.has('disconnect'):
		for f in hooks['disconnect']:
			f.call_func()

func _connection_error(err):
	is_connected = false
	print("Connection error: ", err)
	if hooks.has('error'):
		for f in hooks['error']:
			f.call_func()
			
func _hooks_cleanup():
	for k in hooks.keys():
		var indexes_to_remove = []
		for i in range(hooks[k].size()):
			if !hooks[k][i].is_valid():
				indexes_to_remove.append(i)
		for index in indexes_to_remove:
			hooks[k].remove(index)
	
func connect_to_server():
	print("Connecting to " + websocket_url)
	ws.connect_to_url(websocket_url)
	
func on(event_name, func_ref):
	if not hooks.has(event_name):
		hooks[event_name] = []
	hooks[event_name].append(func_ref)
	
func emit(event_name, args):
	args.push_front(event_name)
	var message = JSON.print(args)
	ws.get_peer(1).put_packet(message.to_utf8())
