extends Control

const player_list_item = preload("res://scenes/PlayerListItem.tscn")
const ready_icon_on = preload("res://images/ready_icon_on.png")
const ready_icon_off = preload("res://images/ready_icon_off.png")

var player_list_position = {}

var game_starting = false

func _ready():
	GlobalData.my_player.ready = false
	GlobalData.players[GlobalData.my_player.playerNumber].ready = false
	Socket.on('player-joined', funcref(self, '_on_player_joined'))
	Socket.on('player-left', funcref(self, '_on_player_left'))
	Socket.on('player-update', funcref(self, '_on_player_update'))
	Socket.on('game-error', funcref(self, '_on_game_error'))
	Socket.on('game-starting', funcref(self, '_on_game_starting'))
	
	$GameCodeContainer/GameCode.text = GlobalData.server_code
	_refresh_player_list()

func _on_ReadyButton_button_up():
	GlobalData.my_player.ready = !GlobalData.my_player.ready
	GlobalData.players[GlobalData.my_player.playerNumber].ready = GlobalData.my_player.ready
	Socket.emit('set-player-ready', [GlobalData.my_player.ready])
	if GlobalData.my_player.ready:
		$ReadyButton.text = "Unready"
	else:
		$ReadyButton.text = "Ready"

func _on_ExitGameButton_button_up():
	Socket.emit('exit-game', [])
	GlobalData.server_code = ""
	GlobalData.players = {}
	get_parent().change_screen(GlobalData.constants.SCREEN.MAIN_MENU)
	
func _on_StartGameButton_button_up():
	if !game_starting:
		game_starting = true
		Socket.emit('start-game', [])
	
func _on_player_left(playerNumber, game_update):
	GlobalData.player_left(playerNumber)
	GlobalData.update_game(game_update)
	_refresh_player_list()
	
func _on_player_joined(playerNumber, game_update):
	GlobalData.update_game(game_update)
	_refresh_player_list()

func _on_player_update(playerData):
	GlobalData.update_player(playerData)
	_refresh_player_list()
	
func _on_game_error(err):
	game_starting = false
	
func _on_game_starting():
	get_parent().change_screen(GlobalData.constants.SCREEN.ADVENTURE)
	
func _refresh_player_list():
	for c in $ScrollContainer/PanelContainer/PlayersContainer.get_children():
		c.queue_free()
		
	for i in range(GlobalData.players.keys().size()):
		var playerNumber = GlobalData.players.keys()[i]
		player_list_position[playerNumber] = i
		var p = GlobalData.players[playerNumber]
		var pl_item = player_list_item.instance()
		$ScrollContainer/PanelContainer/PlayersContainer.add_child(pl_item)
		pl_item.get_node("./Panel/HBoxContainer/PlayerName").text = p.nickname
		pl_item.get_node("./Panel/HBoxContainer/PlayerName").add_color_override("font_color", p.playerColor)
		pl_item.get_node("./Panel/HBoxContainer/ClassImage").texture = GlobalData.constants.char_classes[int(p.characterClass)].icon
		pl_item.get_node("./Panel/HBoxContainer/ReadyIcon").texture = ready_icon_on if p.ready else ready_icon_off


