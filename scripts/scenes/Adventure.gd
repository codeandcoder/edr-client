extends Control

func _ready():
	Socket.on('game-update', funcref(self, '_on_game_update'))
	Socket.emit('game-screen-loaded', [])
	
func _on_game_update(game_update):
	GlobalData.update_game(game_update)
	$EventTitle.text = game_update.eventTitle
	$EventText.text = game_update.eventText
	
	if game_update.isChoice:
		configure_choices(game_update.options)
	else:
		configure_continue()
		
func configure_choices(options):
	$ButtonsContainer/MarginContainer2/Button2.visible = false
	$ButtonsContainer/MarginContainer3/Button3.visible = false
	$ButtonsContainer/MarginContainer4/Button4.visible = false
	
	for i in range(1, options.keys().size()+1):
		var optionID = options.keys()[i-1]
		$ButtonsContainer.get_node("./MarginContainer" + str(i) + "/Button" + str(i)).visible = true
		$ButtonsContainer.get_node("./MarginContainer" + str(i) + "/Button" + str(i)).text = options[optionID].text
	
func configure_continue():
	$ButtonsContainer/MarginContainer1/Button1.text = "Continuar"
	$ButtonsContainer/MarginContainer2/Button2.visible = false
	$ButtonsContainer/MarginContainer3/Button3.visible = false
	$ButtonsContainer/MarginContainer4/Button4.visible = false

func _on_Button1_button_up():
	if GlobalData.last_game_update.isChoice:
		var optionID = GlobalData.last_game_update.options.keys()[0]
		Socket.emit('choose-option', [optionID])
	else:
		Socket.emit('continue-game', [])

func _on_Button2_button_up():
	if GlobalData.last_game_update.isChoice:
		var optionID = GlobalData.last_game_update.options.keys()[1]
		Socket.emit('choose-option', [optionID])

func _on_Button3_button_up():
	if GlobalData.last_game_update.isChoice:
		var optionID = GlobalData.last_game_update.options.keys()[2]
		Socket.emit('choose-option', [optionID])

func _on_Button4_button_up():
	if GlobalData.last_game_update.isChoice:
		var optionID = GlobalData.last_game_update.options.keys()[3]
		Socket.emit('choose-option', [optionID])
