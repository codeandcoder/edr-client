extends Control

var joining = false

func _ready():
	if GlobalData.my_player.playerID == null:
		get_parent().change_screen(GlobalData.constants.SCREEN.CHARACTER_CREATION)
		
	Socket.on('game-joined', funcref(self, '_on_game_joined'))
	Socket.on('game-error', funcref(self, '_on_game_error'))

func _on_HostGame_button_up():
	GlobalData.players = {}
	GlobalData.last_game_update = null
	if Socket.is_connected and !joining:
		$HostGame.disabled = true
		$JoinGame.disabled = true
		$EditCharacter.disabled = true
		joining = true
		Socket.emit('host-game', [GlobalData.my_player.serialise()])
		
func _on_JoinGame_button_up():
	GlobalData.players = {}
	GlobalData.last_game_update = null
	if Socket.is_connected and !joining:
		$HostGame.disabled = true
		$JoinGame.disabled = true
		$EditCharacter.disabled = true
		joining = true
		Socket.emit('join-game', [$GameCode.text, GlobalData.my_player.serialise()])

func _on_EditCharacter_button_up():
	get_parent().change_screen(GlobalData.constants.SCREEN.CHARACTER_CREATION)

func _on_game_joined(own_player, game_update):
	GlobalData.server_code = game_update.gameCode
	GlobalData.update_player(own_player)
	for p in game_update.players:
		GlobalData.update_player(p)
	get_parent().change_screen(GlobalData.constants.SCREEN.LOBBY)
	
func _on_game_error(error):
	joining = false
	$HostGame.disabled = false
	$JoinGame.disabled = false
	$EditCharacter.disabled = false

