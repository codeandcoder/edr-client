extends Control

const screen_resources = {
	GlobalData.constants.SCREEN.MAIN_MENU : preload("res://scenes/MainMenu.tscn"),
	GlobalData.constants.SCREEN.CHARACTER_CREATION : preload("res://scenes/CharacterCreation.tscn"),
	GlobalData.constants.SCREEN.LOBBY : preload("res://scenes/Lobby.tscn"),
	GlobalData.constants.SCREEN.ADVENTURE : preload("res://scenes/Adventure.tscn")
}
const initial_screen = GlobalData.constants.SCREEN.MAIN_MENU

var current_screen = null

func _ready():
	$HBoxContainer/ConnectionStatus.add_color_override("font_color", Color(1,0,0))
	
	Socket.on('connect', funcref(self, '_connected_to_server'))
	Socket.on('disconnect', funcref(self, '_disconnected'))
	Socket.on('game-error', funcref(self, '_game_error'))
	Socket.on('any', funcref(self, '_any_message'))
	Socket.connect_to_server()
	
	change_screen(initial_screen)

func _connected_to_server(protocol):
	$HBoxContainer/ConnectionStatus.text = "On-Line"
	$HBoxContainer/ConnectionStatus.add_color_override("font_color", Color(0,1,0))
	
func _disconnected():
	change_screen(GlobalData.constants.SCREEN.MAIN_MENU)
	$HBoxContainer/ConnectionStatus.text = "Off-Line"
	$HBoxContainer/ConnectionStatus.add_color_override("font_color", Color(1,0,0))

func _game_error(error):
	$ErrorMessage.text = "Error: " + error
	
func _any_message(eventName):
	if eventName != "game-error":
		$ErrorMessage.text = ""
	
func change_screen(screen):
	if current_screen != null:
		current_screen.queue_free()
	current_screen = screen_resources[screen].instance()
	add_child(current_screen)
	move_child(current_screen, 0)

func _on_ReconnectionTimer_timeout():
	if !Socket.is_connected:
		Socket.connect_to_server()
