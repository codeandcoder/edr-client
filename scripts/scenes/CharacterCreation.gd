extends Control

const uuid_util = preload('res://scripts/uuid.gd')

func _ready():
	for c_id in GlobalData.constants.char_classes.keys():
		$ClassContainer/CharacterClassButton.add_item(GlobalData.constants.char_classes[c_id].name, c_id)
	
	if !GlobalData.my_player.playerID == null:
		$CharacterNameContainer/CharacterName.set_text(GlobalData.my_player.nickname)
		$ClassContainer/CharacterClassButton.select(GlobalData.my_player.characterClass)
	
func _on_SaveButton_button_up():
	var my_player = GamePlayer.new()
	my_player.playerID = uuid_util.v4()
	my_player.nickname = $CharacterNameContainer/CharacterName.text
	var selected_class = $ClassContainer/CharacterClassButton.get_selected_id()
	my_player.characterClass = selected_class
	my_player.maxHealth = GlobalData.constants.char_classes[selected_class].maxHealth
	my_player.currentHealth = my_player.maxHealth
	my_player.maxEnergy = GlobalData.constants.char_classes[selected_class].maxEnergy
	my_player.currentEnergy = my_player.maxEnergy
	my_player.attackValue = GlobalData.constants.char_classes[selected_class].attackValue
			
	GlobalData.create_player(my_player)
	get_parent().change_screen(GlobalData.constants.SCREEN.MAIN_MENU)
