extends Node

const constants = preload("res://scripts/constants.gd")

var server_code = null
var my_player = null
var players = {}

var last_game_update = null

func _ready():
	my_player = GamePlayer.new()
	if my_player.has_char_data(constants.char_data_savefile):
		my_player.load_char_data(constants.char_data_savefile)
	
func create_player(new_player):
	my_player = new_player
	my_player.save_char_data(constants.char_data_savefile)
	
func player_left(playerNumber):
	players.erase(playerNumber)
	
func update_game(game_update):
	last_game_update = game_update
	for p in game_update.players:
		update_player(p)
	
func update_player(player):
	if !players.has(player.playerNumber):
		players[player.playerNumber] = GamePlayer.new()
		players[player.playerNumber].playerColor = GlobalData.constants.player_colors[players.keys().size()-1]
		
	players[player.playerNumber].parse_data(player)
	
	if player.has("playerID"):
		my_player.parse_data(player)
		
	if player.playerNumber == my_player.playerNumber:
		my_player.parse_data(player)
