extends Reference

enum SCREEN {MAIN_MENU, CHARACTER_CREATION, LOBBY, ADVENTURE}

const char_classes = {
	0: {
		"id": 0,
		"name": "Mage",
		"maxHealth": 8,
		"maxEnergy": 5,
		"attackValue": 2,
		"icon": preload("res://images/mage_icon.png")
	},
	1: {
		"id": 1,
		"name": "Warrior",
		"maxHealth": 12,
		"maxEnergy": 4,
		"attackValue": 8,
		"icon": preload("res://images/warrior_icon.png")
	},
	2: {
		"id": 2,
		"name": "Bard",
		"maxHealth": 10,
		"maxEnergy": 3,
		"attackValue": 4,
		"icon": preload("res://images/bard_icon.png")
	},
	3: {
		"id": 3,
		"name": "Ranger",
		"maxHealth": 10,
		"maxEnergy": 4,
		"attackValue": 6,
		"icon": preload("res://images/ranger_icon.png")
	}
}

const player_colors = [
	Color(0,0.4,0.9),  #Blue
	Color(0,0.7,0.1),  #Green
	Color(0.6,0.1,1),  #Purple
	Color(0.6,0.2,0.2), #Red
	Color(0.9,0.9,0.3), #Yellow
	Color(0.9,0.6,0.1), #Orange
	Color(0.7,0.9,1),	#Cyan
	Color(0,0.4,0.9),  #Blue
	Color(0,0.7,0.1),  #Green
	Color(0.6,0.1,1)  #Purple
]

const char_data_savefile = "user://character.save"
