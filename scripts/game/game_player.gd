class_name GamePlayer
extends Reference

var playerID
var playerNumber
var nickname
var characterClass
var maxHealth
var currentHealth
var maxEnergy
var currentEnergy
var attackValue
var ready = false
var playerColor

func serialise():
	return {
		"playerID": playerID,
		"playerNumber": playerNumber,
		"nickname": nickname,
		"characterClass": characterClass,
		"maxHealth": maxHealth,
		"currentHealth": currentHealth,
		"maxEnergy": maxEnergy,
		"currentEnergy": currentEnergy,
		"attackValue": attackValue,
		"ready": ready
	}
	
func parse_data(dict):
	for d in dict.keys():
		self.set(d, dict[d])
	currentHealth = maxHealth
	currentEnergy = maxEnergy
	# Only for test purposes
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	playerID = str(rng.randi())

func has_char_data(file_url):
	var char_file = File.new()
	return char_file.file_exists(file_url)

func load_char_data(file_url):
	var char_file = File.new()
	if char_file.file_exists(file_url):
		char_file.open(file_url, File.READ)
		var data = char_file.get_line()
		parse_data(parse_json(data))
		char_file.close()
			
func save_char_data(file_url):
	var char_file = File.new()
	char_file.open(file_url, File.WRITE)
	char_file.store_line(to_json(serialise()))
	char_file.close()
